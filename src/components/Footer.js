import React from 'react';
import {
    MDBFooter,
    MDBContainer,
    MDBIcon,
    MDBInput,
    MDBCol,
    MDBRow,
    MDBBtn
} from 'mdb-react-ui-kit';

export default function App() {
    return (
        <MDBFooter className='text-center mt-5' color='white' bgColor='dark'>
            <MDBContainer className='p-4'>
                <section className='mb-4'>
                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='facebook-f' />
                    </MDBBtn>

                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='twitter' />
                    </MDBBtn>

                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='google' />
                    </MDBBtn>

                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='instagram' />
                    </MDBBtn>

                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='linkedin-in' />
                    </MDBBtn>

                    <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
                        <MDBIcon fab icon='github' />
                    </MDBBtn>
                </section>

                <section className=''>
                    <form action=''>
                        <MDBRow className='d-flex justify-content-center'>
                            <MDBCol size="auto">
                                <p className='pt-2'>
                                    <strong>Sign up for our newsletter</strong>
                                </p>
                            </MDBCol>

                            <MDBCol md='5' start='12'>
                                <MDBInput contrast type='email' className='mb-4' placeholder='Email address' />
                            </MDBCol>

                            <MDBCol size="auto">
                                <MDBBtn outline color='light' type='submit' className='mb-4'>
                                    Subscribe
                                </MDBBtn>
                            </MDBCol>
                        </MDBRow>
                    </form>
                </section>

                <section className='mb-4'>
                    <p>

                    </p>
                </section>

                <section className=''>
                    <MDBRow>
                        <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                            <h5 className='text-uppercase'><MDBIcon icon="gem" className="me-3" />Funko Papster!</h5>

                            <ul className='list-unstyled mb-0'>
                                <li>
                                    <p className='text-white'>
                                        Funko Papsters! delivers the widest array of exclusives, chase, and special edition Funko Pops to fans across the Philippines.
                                    </p>
                                </li>
                            </ul>
                        </MDBCol>

                        <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                            <h5 className='text-uppercase'>About</h5>

                            <ul className='list-unstyled mb-0'>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Newsroom
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Careers
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Store Locator
                                    </a>
                                </li>
                            </ul>
                        </MDBCol>

                        <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                            <h5 className='text-uppercase'>Support</h5>

                            <ul className='list-unstyled mb-0'>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        FAQ
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Membership Program
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Cancellation Policy
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Returns Policy
                                    </a>
                                </li>
                                <li>
                                    <a style={{textDecoration: 'none'}} href='#!' className='text-white'>
                                        Replacement Policy
                                    </a>
                                </li>
                            </ul>
                        </MDBCol>

                        <MDBCol lg='3' md='6' className='mb-4 mb-md-0'>
                            <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
                            <p>
                                <MDBIcon icon="home" className="me-2" />
                                San Juan, Metro Manila 1500, PH
                            </p>
                            <p>
                                <MDBIcon icon="envelope" className="me-3" />
                                funkopapster@mail.com
                            </p>
                            <p>
                                <MDBIcon icon="phone" className="me-3" /> +63 912 345 6789
                            </p>
                            <p>
                                <MDBIcon icon="print" className="me-3" />  (02) 8123 45 67
                            </p>
                        </MDBCol>
                    </MDBRow>
                </section>
            </MDBContainer>

            <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                © 2022 Copyright: Funko Papster!
            </div>
        </MDBFooter>
    );
}