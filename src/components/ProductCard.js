import { useState, useEffect } from "react";

import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBCard,
    MDBCardBody,
    MDBCardImage,
    MDBBtn
} from "mdb-react-ui-kit";


export default function ProductCard({ productProp }) {
    const { _id, productName, description, price, stocks, imgSrc } = productProp;

    return (
        <MDBContainer fluid>
            <MDBRow className="mb-0">
                <MDBCol md="12">
                    <MDBCard className="shadow-0 border rounded-3 mt-5 mb-3">
                        <MDBCardBody>
                            <MDBCardImage
                                src={imgSrc}
                                fluid
                                className="w-100"
                                style={{ height: 200 }}
                            />
                            <h5>{productName}</h5>

                            <div className="mt-1 mb-0 text-muted small">
                                <p>{description}</p>
                            </div>
                            <div className="d-flex flex-row align-items-center mb-1">
                                <h4 className="mb-1 me-1">₱{price}</h4>
                                <span className="text-danger">
                                    <s>₱{price + 500}</s>
                                </span>
                            </div>
                            <h6 className="text-success">Free shipping</h6>
                            <div className="mt-1 mb-0 text-muted small">
                                <p>Available: {stocks}</p>
                            </div>
                            <div className="d-flex flex-column mt-4">
                                <Button as={Link} to={`/products/getProduct/${_id}`} color="dark" size="sm" variant="dark">
                                    Details
                                </Button>
                                <Button outline color="dark" size="sm" className="mt-2" variant="outline-dark">
                                    Add to cart
                                </Button>
                            </div>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    )
}