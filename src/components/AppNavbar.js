
import { useState, useContext } from "react";

import { NavLink } from "react-router-dom";

import { Container, Nav, Navbar, Form } from 'react-bootstrap';

import UserContext from "../UserContext";

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
    <Navbar bg="" expand="lg">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/" className="ms-5">Funko Papster!</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="bg-white" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto ps-5">
            <Nav.Link className="px-5" as={NavLink} to="/" end>Home</Nav.Link>
            {
              (user.isAdmin)
                ?
                <Nav.Link as={NavLink} to="/admin" end>Admin</Nav.Link>
                :
                <>
                  <Nav.Link as={NavLink} to="/products" end>Products</Nav.Link>
                  <Nav.Link className="px-5" as={NavLink} to="/new" end>New Arrivals!</Nav.Link>
                </>
            }
            {/* <Nav.Link as={NavLink} to="/register" end>Register</Nav.Link> */}
          </Nav>
          <Form className="d-flex me-5">
            <Form.Control
              type="search"
              placeholder="Search funkopop"
              className="me-2"
              aria-label="Search"
            />
            <Nav className="">
              {
                (user.id !== null)
                  ?
                  <Nav.Link className="px-3" as={NavLink} to="/logout" end><i class="fa-solid fa-right-from-bracket"></i></Nav.Link>
                  :
                  <Nav.Link className="px-3" as={NavLink} to="/login" end><i class="fa-solid fa-user-large"></i></Nav.Link>
              }
              <Nav.Link className="px-3" as={NavLink} to="/cart" end><i class="fa-solid fa-cart-shopping"></i></Nav.Link>
              <Nav.Link className="px-3" as={NavLink} to="/customerService" end><i class="fa-solid fa-headset"></i></Nav.Link>
            </Nav>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}