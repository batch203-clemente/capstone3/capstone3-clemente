import { useEffect, useState, useContext } from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

import {
    MDBContainer,
    MDBRow,
    MDBCol,
    MDBCard,
    MDBCardBody,
    MDBCardImage,
    MDBBtn,
    MDBRipple
} from "mdb-react-ui-kit";

export default function ProductView() {

    const { user } = useContext(UserContext);

    const { productId } = useParams();
    const navigate = useNavigate();

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [imgSrc, setImgSrc] = useState('');
    const [quantity, setQuantity] = useState(1);

    useEffect(() => {
        console.log(productId);
        fetch(`${process.env.REACT_APP_API_URL}/products/getProduct/${productId}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setProductName(data.productName);
                setDescription(data.description);
                setPrice(data.price);
                setStocks(data.stocks);
                setImgSrc(data.imgSrc);
            });
    }, [productId])

    const checkout = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                totalAmount: price,
                products: [
                    {
                        productId: productId,
                        productName: productName,
                        quantity: quantity
                    }
                ]
            })
        })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Checkout Successful",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        icon: "success",
                        text: "You have successfully bought this product"
                    })
                    navigate("/products");
                } else {
                    Swal.fire({
                        title: "Oops! Something went wrong",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        icon: "error",
                        text: "Please try again."
                    })
                }
            });
    }


    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <MDBCardImage
                                src={imgSrc}
                                fluid
                                className="w-75 mb-3"
                                style={{ height: 200 }}
                            />
                            <Card.Title className="mb-3">{productName}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Text className="text-muted small">Available Stocks: {stocks}</Card.Text>
                            {
                                (user.id !== null)
                                    ?
                                    <div className="d-flex flex-column mt-4">
                                    <Button variant="outline-dark" size="md" onClick={() => checkout(productId)}>Checkout</Button>
                                    </div>
                                    :
                                    <div className="d-flex flex-column mt-4">
                                    <Button as={Link} to="/login" variant="outline-dark" size="md">Login to Checkout</Button>
                                    </div>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}