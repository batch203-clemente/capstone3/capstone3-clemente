
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
// import homeImg from "./images/robin.jpg";

export default function Home() {

    const products = "/products";

    return (
        <>
            <div className="bg my-5">
                <div className="fg text-white pt-5 ps-5">
                    <h4 className="mt-5">The wait is over!</h4>
                    <h1 className="w-50">New funko pop collectibles available only here</h1>
                    <h3 className="my-3">Grab yours now</h3>
                    <Link to={products}>
                        <Button variant="outline-light">Shop Collection</Button>
                    </Link>
                </div>
            </div>
        </>
    )
}