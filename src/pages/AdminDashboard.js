import { useContext, useEffect, useState } from "react";

import { Navigate } from 'react-router-dom';


import { Button, Table, Modal, Form } from "react-bootstrap";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDashboard() {

    const { user } = useContext(UserContext);

    const [allProducts, setAllProducts] = useState([]);

    const [productId, setProductId] = useState("");
    const [productName, setProductName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);

    const [isActive, setIsActive] = useState(false);

    const [showAdd, setShowAdd] = useState(false);
    const [showEdit, setShowEdit] = useState(false);


    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);

    const openEdit = (id) => {
        setProductId(id);

        fetch(`${process.env.REACT_APP_API_URL}/products/getProduct/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setProductName(data.productName);
                setDescription(data.description);
                setPrice(data.price);
                setStocks(data.stocks);
            });

        setShowEdit(true)
    };

    const closeEdit = () => {

        // Clear input fields upon closing the modal
        setProductName('');
        setDescription('');
        setPrice(0);
        setStocks(0);

        setShowEdit(false);
    };


    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setAllProducts(data.map(product => {
                    return (
                        <tr key={product._id}>
                            <td>{product._id}</td>
                            <td>{product.productName}</td>
                            <td>{product.description}</td>
                            <td>{product.price}</td>
                            <td>{product.stocks}</td>
                            <td>{product.isActive ? "Active" : "Inactive"}</td>
                            <td>
                                {
                                    (product.isActive)
                                        ?
                                        <Button variant="outline-danger" size="sm" onClick={() => archive(product._id, product.productName)}>Archive</Button>
                                        :
                                        <>
                                            <Button variant="outline-success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.productName)}>Unarchive</Button>
                                            <Button variant="outline-secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
                                        </>
                                }
                            </td>
                        </tr>
                    )
                }))
            })
    }

    useEffect(() => {
        fetchData();
    }, [])

    const archive = (id, productName) => {
        console.log(id);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Archive Successful",
                        icon: "success",
                        text: `${productName} is now inactive`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    fetchData();
                } else {
                    Swal.fire({
                        title: "Archive Failed",
                        icon: "error",
                        text: `Something went wrong. Please try again later.`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            })
    }

    const unarchive = (id, productName) => {
        console.log(id);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Unarchive Successful",
                        icon: "success",
                        text: `${productName} is now active`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    fetchData();
                } else {
                    Swal.fire({
                        title: "Unarchive Failed",
                        icon: "error",
                        text: `Something went wrong. Please try again later.`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            })
    }

    const addProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName: productName,
                description: description,
                price: price,
                stocks: stocks
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Product succesfully Added",
                        icon: "success",
                        text: `${productName} is now added`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });


                    fetchData();
                    closeAdd();
                }
                else {
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    closeAdd();
                }

            })

        setProductName('');
        setDescription('');
        setPrice(0);
        setStocks(0);
    }

    const editProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productName: productName,
                description: description,
                price: price,
                stocks: stocks
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Course succesfully Updated",
                        icon: "success",
                        text: `${productName} is now updated`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });

                    fetchData();
                    closeEdit();

                }
                else {
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });

                    closeEdit();
                }

            })

        setProductName('');
        setDescription('');
        setPrice(0);
        setStocks(0);
    }

    useEffect(() => {

        if (productName != "" && description != "" && price > 0 && stocks > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productName, description, price, stocks]);


    return (
        (user.isAdmin)
            ?
            <>
                <div className="mt-5 mb-3 text-center">
                    <h1>Admin Dashboard</h1>
                </div>

                <Table bordered className="text-white">
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Stocks</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allProducts}
                    </tbody>
                </Table>
                <div className="my-5 text-center">
                    <Button variant="outline-dark" className="mx-2" onClick={openAdd}>Add Products</Button>
                    <Button variant="outline-secondary" className="mx-2">Show Orders</Button>
                </div>

                <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
                    <Form onSubmit={e => addProduct(e)}>

                        <Modal.Header closeButton>
                            <Modal.Title>Add New Product</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form.Group controlId="name" className="mb-3">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Name"
                                    value={productName}
                                    onChange={e => setProductName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="stocks" className="mb-3">
                                <Form.Label>Product Stocks</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Slots"
                                    value={stocks}
                                    onChange={e => setStocks(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeAdd}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>

                <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
                    <Form onSubmit={e => editProduct(e)}>

                        <Modal.Header closeButton>
                            <Modal.Title>Edit a Product</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form.Group controlId="productName" className="mb-3">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Product Name"
                                    value={productName}
                                    onChange={e => setProductName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Product Description"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="stocks" className="mb-3">
                                <Form.Label>Product Stocks</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Product Stocks"
                                    value={stocks}
                                    onChange={e => setStocks(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeEdit}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>


            </>
            :
            <Navigate to="/products" />
    )
}