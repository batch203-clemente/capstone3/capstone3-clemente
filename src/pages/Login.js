import React from "react";
import { useState, useEffect, useContext } from "react";

import { Navigate } from "react-router-dom";
import { Link } from "react-router-dom"
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

import loginImg from "./images/Freddy1.png";

export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password])

    function authenticate(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data.accessToken);

                if (data.accessToken !== undefined) {
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    // let timerInterval;
                    Swal.fire({
                        title: "Login Successful",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        // timerProgressBar: true,
                        icon: "success",
                        text: "Welcome to Funko Papster!"
                    })
                } else {
                    Swal.fire({
                        title: "Authentication failed",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        // timerProgressBar: true,
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
            })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    return (
        (user.id !== null)
            ?
            <Navigate to="/products" />
            :
            <>
                {/* <h1 className="my-5 text-center">Login</h1> */}
                <div className="w-50 align-center m-auto py-5">
                    <div className="text-center">
                        <img className="mb-4 " style={{ width: 200, height: 300 }} src={loginImg} alt="" />
                    </div>

                    <Form onSubmit={e => { authenticate(e) }}>
                        <Form.Group className="mb-3" controlId="emailAddress">
                            {/* <Form.Label>Email Address</Form.Label> */}
                            <Form.Control
                                type="email"
                                placeholder="Email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            {/* <Form.Label>Password</Form.Label> */}
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <div className="d-flex align-items-center justify-content-end">
                            <Link style={{ textDecoration: 'none' }} className="mb-3">Forgot password?</Link>
                        </div>

                        <div className="d-flex align-items-center justify-content-center">
                            {
                                isActive
                                    ?
                                    <Button className="" variant="outline-dark" type="submit" id="sumbitBtn">
                                        Login
                                    </Button>
                                    :
                                    <Button className="" variant="outline-danger" type="submit" id="sumbitBtn" disabled>
                                        Login
                                    </Button>
                            }
                        </div>

                        <div className="d-flex align-items-center justify-content-center">
                            <Form.Text className="me-2 mt-2">
                                Not registered yet?
                                <Link style={{ textDecoration: 'none' }} to="/register" className="ms-1">Sign up now</Link>
                            </Form.Text>
                        </div>
                    </Form>
                </div>
            </>
    )
}