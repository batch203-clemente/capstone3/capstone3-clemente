import { useState, useEffect, useContext } from "react";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";

import React from "react";

import { Navigate } from "react-router-dom";

// import "./ecommerce-category-product.css";

export default function Products() {

    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allActiveProducts`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setProducts(data.map(product => {
                    return (
                        <ProductCard key={product._id} productProp={product} />
                    )
                }))
            })
    }, [])

    return (
        (user.isAdmin)
            ?
            <Navigate to="/admin" />
            :
            <>
                <h1 className="text-center mt-5">Products</h1>
                <div className="d-flex col-12">
                    {products}
                </div>
            </>
    );
}