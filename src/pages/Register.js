import { useEffect, useState, useContext } from "react";

import { Navigate, useNavigate, Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Register() {


    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate();

    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if ((fName !== '' && lName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])

    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data) {
                    Swal.fire({
                        title: "Duplicate email found",
                        position: "top",
                        background: "#FFFAE7",
                        timer: 2000,
                        showConfirmButton: false,
                        icon: "error",
                        text: "Kindly provide another email to complete registration."
                    })
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/checkMobile`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            mobileNumber: mobileNo
                        })
                    })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);
                            if (data) {
                                Swal.fire({
                                    title: "Duplicate mobile number found",
                                    position: "top",
                                    background: "#FFFAE7",
                                    timer: 2000,
                                    showConfirmButton: false,
                                    icon: "error",
                                    text: "Kindly provide another mobile number to complete registration."
                                })
                            } else {
                                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                                    method: "POST",
                                    headers: {
                                        "Content-Type": "application/json"
                                    },
                                    body: JSON.stringify({
                                        firstName: fName,
                                        lastName: lName,
                                        email: email,
                                        password: password1,
                                        mobileNumber: mobileNo
                                    })
                                })
                                    .then(res => res.json())
                                    .then(data => {
                                        console.log(data);

                                        if (data) {
                                            Swal.fire({
                                                title: "Registration Successful",
                                                icon: "success",
                                                text: "Welcome to Funko Papster!",
                                                position: "top",
                                                background: "#FFFAE7",
                                                showConfirmButton: false,
                                                timer: 2000
                                            });

                                            setFName('');
                                            setLName('');
                                            setEmail('');
                                            setMobileNo('');
                                            setPassword1('');
                                            setPassword2('');

                                            navigate("/login");
                                        } else {
                                            Swal.fire({
                                                title: "Something went wrong",
                                                icon: "error",
                                                text: "Please try again.",
                                                position: "top",
                                                background: "#FFFAE7",
                                                showConfirmButton: false,
                                                timer: 2000
                                            });
                                        }
                                    })
                            }
                        })
                }
            })
    }


    return (
        <>
            <h1 className="my-5 text-center">Register</h1>
            <div className="w-50 align-center m-auto">
                <Form onSubmit={e => { registerUser(e) }}>
                    <Form.Group className="mb-3 d-flex" controlId="name">
                        {/* <Form.Label>First Name</Form.Label> */}
                        <Form.Control
                            className="me-2"
                            type="text"
                            placeholder="Enter first name"
                            value={fName}
                            onChange={e => setFName(e.target.value)}
                            required
                        />

                        {/* <Form.Label>Last Name</Form.Label> */}
                        <Form.Control
                            className="ms-2"
                            type="text"
                            placeholder="Enter last name"
                            value={lName}
                            onChange={e => setLName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="emailAddress">
                        {/* <Form.Label>Email Address</Form.Label> */}
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="mobileNo">
                        {/* <Form.Label>Mobile No.</Form.Label> */}
                        <Form.Control
                            type="number"
                            placeholder="09XXXXXXXXX"
                            value={mobileNo}
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your mobile no. with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3 d-flex" controlId="password">
                        {/* <Form.Label>Password</Form.Label> */}
                        <Form.Control
                            className="me-2"
                            type="password"
                            placeholder="Enter password"
                            value={password1}
                            onChange={e => setPassword1(e.target.value)}
                            required
                        />
                        <Form.Control
                            className="ms-2"
                            type="password"
                            placeholder="Verify password"
                            value={password2}
                            onChange={e => setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3 d-flex" controlId="password2">

                    </Form.Group>

                    <Form.Group className="mb-3" controlId="fillOut">
                        <Form.Text>All fields are required</Form.Text>
                    </Form.Group>

                    {
                        isActive
                            ?
                            <Button variant="outline-dark" type="submit" id="sumbitBtn">
                                Submit
                            </Button>
                            :
                            <Button variant="danger" type="submit" id="sumbitBtn" disabled>
                                Submit
                            </Button>
                    }
                    <div className="d-flex">
                        <Form.Text className=" me-2 mt-2">
                            Already have an account?
                            <Link style={{ textDecoration: 'none' }} to="/login" className="mx-1">Click here</Link>
                            to login.
                        </Form.Text>
                    </div>
                </Form>
            </div>
        </>
    )

}