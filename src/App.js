import { useEffect, useState } from "react";

import 'bootswatch/dist/sketchy/bootstrap.min.css';

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";

import AppNavbar from "./components/AppNavbar";
import Footer from "./components/Footer";

import AdminDashboard from "./pages/AdminDashboard";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";

import './App.css';
import { UserProvider } from "./UserContext";


export default function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    // Allows us to clear the information in the localStrage
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
  }, [])

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/admin" element={<AdminDashboard />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/getProduct/:productId" element={<ProductView />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/register" element={<Register />} />
            {/* <Route exact path="*" element={<PageNotFound />} /> */}
          </Routes>
        </Container>
        <Footer />
      </Router>
    </UserProvider>
  );
}
